package com.bolsadeideas.springboot.springboot.app.controller;

import com.bolsadeideas.springboot.springboot.app.editor.NombreMayusculaEditor;
import com.bolsadeideas.springboot.springboot.app.editor.PaisPropertyEditor;
import com.bolsadeideas.springboot.springboot.app.editor.RolesEditor;
import com.bolsadeideas.springboot.springboot.app.models.Pais;
import com.bolsadeideas.springboot.springboot.app.models.Role;
import com.bolsadeideas.springboot.springboot.app.models.Usuario;
import com.bolsadeideas.springboot.springboot.app.services.PaisService;
import com.bolsadeideas.springboot.springboot.app.services.RoleService;
import com.bolsadeideas.springboot.springboot.app.validation.UsuarioValidador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;


@Controller
@SessionAttributes("usuario")
public class FormController {

    @Autowired
    private UsuarioValidador validador;

    @Autowired
    private PaisService paisService;

    @Autowired
    private PaisPropertyEditor paisEditor;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RolesEditor rolesEditor ;

    @InitBinder
    public void initBinder(WebDataBinder binder){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class,"fechaNacimiento", new CustomDateEditor(dateFormat, false));
        // filtro personlaizado para poner en mayuscula solo el atributo "nombre"
        binder.registerCustomEditor(String.class, "nombre",new NombreMayusculaEditor());

        binder.registerCustomEditor(Pais.class, "pais", paisEditor);

        binder.registerCustomEditor(Role.class, "roles", rolesEditor);

    }

    @ModelAttribute("genero")
    public List<String> genero(){
        return Arrays.asList("Hombre", "Mujer");
    }

    @ModelAttribute("listaRoles")
    public List<Role> listaRoles(){
        return this.roleService.listar();
    }

    @ModelAttribute("listaPaises")
    public List<Pais> listaPaises(){
        return paisService.listar();
    }

    @ModelAttribute("listaRolesString")
    public List<String> listaRolesString(){
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_ADMIN");
        roles.add("ROLE_USER");
        roles.add("ROLE_MODERATOR");

        return roles;
    }

    @ModelAttribute("listaRolesMap")
    public Map<String,String> listaRolesMap(){
        //Map es la interface mientras que HashMap es la implementacion puede ser tambien LinkedHashMap
        Map<String, String> roles = new HashMap<String, String>();
        roles.put("ROLE_ADMIN","Administrador");
        roles.put("ROLE_USER","Usuario");
        roles.put("ROLE_MODERATOR","Moderador");

        return  roles;
    }

    @ModelAttribute("paises")
    public List<String> paises(){
        return Arrays.asList("México","España","Chile","Argentina","Perú","Colombia","Venezuela");
    }

    @ModelAttribute("paisesMap")
    public Map<String,String> paisesMap(){
        //Map es la interface mientras que HashMap es la implementacion puede ser tambien LinkedHashMap
        Map<String, String> paises = new HashMap<String, String>();
        paises.put("MX","México");
        paises.put("ES","España");
        paises.put("CL","Chile");
        paises.put("AR","Argentina");
        paises.put("PE","Perú");
        paises.put("CO","Colombia");
        paises.put("VE","Venezuela");

        return  paises;
    }


    @GetMapping("/form")
    public String form(Model model){
        Usuario usuario = new Usuario();
        usuario.setNombre("Jhon");
        usuario.setApellido("Wick");
        usuario.setIdentificador("123.456.789-K");
        usuario.setHabilitar(true);
        usuario.setValorSecreto("Algún valor secreto...");
        usuario.setPais(new Pais(3,"CL", "Chile"));
        usuario.setRoles(Arrays.asList(new Role(1, "Administrator", "ROLE_ADMIN")));
        model.addAttribute("titulo","Formulario Usuario");
        model.addAttribute("usuario", usuario);
        return "form";
    }

    @PostMapping("/form")
    public String procesar(@Valid Usuario usuario, BindingResult result, Model model) {

        if (result.hasErrors()){
            model.addAttribute("titulo","Resultado form");
            return "form";
        }
        return "redirect:/ver";
    }

    @GetMapping("/ver")
    public String ver(@SessionAttribute(name = "usuario", required = false) Usuario usuario, Model model, SessionStatus status){
        if(usuario == null){
            return "redirect:/form";
        }
        model.addAttribute("titulo","Resultado form");
        status.setComplete();
        return "resultado";
    }
}
