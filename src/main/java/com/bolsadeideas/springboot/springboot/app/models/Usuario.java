package com.bolsadeideas.springboot.springboot.app.models;

import com.bolsadeideas.springboot.springboot.app.validation.IdentificadorRegex;
import com.bolsadeideas.springboot.springboot.app.validation.Requerido;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;
//import javax.validation.constraints.Pattern;


public class Usuario {
    //Se valida en clase personalizada.
    // [\\d] es equivalente a [0-9]
    //@Pattern(regexp = "[0-9]{2}[.][\\d]{3}[.][\\d]{3}[-][A-Z]{1}")
    @IdentificadorRegex
    private String identificador;
    //Se valida en clase personalizada.
    //@NotEmpty(message = "El Nombre no puede ser vacio papu")
    private String nombre;

    //@NotEmpty
    @Requerido
    private String apellido;

    @NotBlank
    @Size(min = 3,max = 8)
    private  String username;

    @NotEmpty
    private String password;

    @NotEmpty
    @Email(message = "Correo con formato incorrecto caon")
    private String email;

    @NotNull
    @Min(5)
    @Max(5000)
    private Integer cuenta;

    @NotNull
    @Past
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fechaNacimiento;

    @NotNull
    private Pais pais;

    @NotEmpty
    private List<Role> roles;

    private Boolean habilitar;

    @NotEmpty
    private String genero;

    private String valorSecreto;

    public Usuario() {
    }

    public Usuario(String identificador, String nombre, String apellido, @NotBlank @Size(min = 3, max = 8) String username, @NotEmpty String password, @NotEmpty @Email(message = "Correo con formato incorrecto caon") String email, @NotNull @Min(5) @Max(5000) Integer cuenta, @NotNull @Past Date fechaNacimiento, @NotNull Pais pais, @NotEmpty List<Role> roles, Boolean habilitar, @NotEmpty String genero, String valorSecreto) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.apellido = apellido;
        this.username = username;
        this.password = password;
        this.email = email;
        this.cuenta = cuenta;
        this.fechaNacimiento = fechaNacimiento;
        this.pais = pais;
        this.roles = roles;
        this.habilitar = habilitar;
        this.genero = genero;
        this.valorSecreto = valorSecreto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {return nombre;}

    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getApellido() { return apellido; }

    public void setApellido(String apellido) { this.apellido = apellido; }

    public String getIdentificador() { return identificador; }

    public void setIdentificador(String identificador) { this.identificador = identificador; }

    public Integer getCuenta() { return cuenta; }

    public void setCuenta(Integer cuenta) { this.cuenta = cuenta; }

    public Date getFechaNacimiento() { return fechaNacimiento; }

    public void setFechaNacimiento(Date fechaNacimiento) { this.fechaNacimiento = fechaNacimiento; }

    public Pais getPais() { return pais; }

    public void setPais(Pais pais) { this.pais = pais; }

    public List<Role> getRoles() { return roles; }

    public void setRoles(List<Role> roles) { this.roles = roles; }

    public Boolean getHabilitar() { return habilitar; }

    public void setHabilitar(Boolean habilitar) { this.habilitar = habilitar; }

    public String getGenero() { return genero; }

    public void setGenero(String genero) { this.genero = genero; }

    public String getValorSecreto() { return valorSecreto; }

    public void setValorSecreto(String valorSecreto) { this.valorSecreto = valorSecreto; }

    @Override
    public String toString() {
        return "Usuario{" +
                "identificador='" + identificador + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", cuenta=" + cuenta +
                ", fechaNacimiento=" + fechaNacimiento +
                ", pais=" + pais +
                ", roles=" + roles +
                ", habilitar=" + habilitar +
                ", genero='" + genero + '\'' +
                ", valorSecreto='" + valorSecreto + '\'' +
                '}';
    }
}
