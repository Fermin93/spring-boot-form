package com.bolsadeideas.springboot.springboot.app.services;

import com.bolsadeideas.springboot.springboot.app.models.Role;

import java.util.List;

public interface RoleService {
    public List<Role> listar();
    public Role obtenerPorId(Integer id);
}
